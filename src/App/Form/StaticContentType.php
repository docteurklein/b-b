<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StaticContentType extends AbstractType
{
    public function getName()
    {
        return 'static_content';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'textarea', [
                'constraints' => [
                    new Assert\NotBlank,
                    new \App\Assert\Twig,
                ]
            ])
            ->add('Modifier', 'submit', [
                'attr' => ['class' => 'btn btn-success']
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'method' => 'PUT',
        ]);
    }
}

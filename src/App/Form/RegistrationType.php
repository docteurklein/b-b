<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

class RegistrationType extends AbstractType
{
    private $places;

    public function __construct(array $places)
    {
        $this->places = $places;
    }

    public function getName()
    {
        return 'registration';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom',
            ])
            ->add('surname', null, [
                'label' => 'Prénom',
            ])
            ->add('email', 'email', [
                'label' => 'e-mail',
            ])
            ->add('startDate', 'date', [
                'label' => 'Date d\'arrivée',
            ])
            ->add('endDate', 'date', [
                'label' => 'Date de départ',
            ])
            ->add('rooms', 'choice', [
                'choices' => array_combine($this->places, $this->places),
                'multiple' => true,
                'expanded' => true,
                'label' => 'Places(s) voulue(s)'
            ])
            ->add('observations', 'textarea', [
                'required' => false,
                'label' => 'Message',
                'attr' => ['class' => 'field span9', 'rows' => 10],
            ])
            ->add('Valider', 'submit', [
                'attr' => ['class' => 'btn btn-success']
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Registration',
            'method' => 'POST',
            'attr' => [
                'class' => 'form form-horizontal',
            ],
        ]);
    }
}

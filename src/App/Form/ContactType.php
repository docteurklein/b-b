<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    public function getName()
    {
        return 'contact';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', ['constraints' => [new Assert\NotBlank]])
            ->add('email', 'email', ['constraints' => [new Assert\Email]])
            ->add('message', 'textarea', ['constraints' => [new Assert\NotBlank]])
            ->add('Envoyer', 'submit', [
                'attr' => ['class' => 'btn btn-success']
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'method' => 'POST',
        ]);
    }
}

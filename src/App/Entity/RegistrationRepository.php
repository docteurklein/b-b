<?php

namespace App\Entity;

use Doctrine\ORM\EntityRepository;

class RegistrationRepository extends EntityRepository
{
    public function findRange(\DateTime $start, \DateTime $end)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.startDate >= :start')
            ->setParameter('start', $start)
            ->andWhere('r.endDate <= :end')
            ->setParameter('end', $end)
            ->getQuery()
            ->execute()
        ;
    }

    public function save(Registration $registration)
    {
        $this->_em->persist($registration);
        $this->_em->flush();
    }
}


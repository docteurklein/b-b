<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Entity\RegistrationRepository")
 */
class Registration
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    public $surname;

    /**
     * @ORM\Column(type="string")
     * @Assert\Email
     */
    public $email;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date
     */
    public $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date
     */
    public $endDate;

    /**
     * @ORM\Column(type="array")
     */
    public $rooms;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    public $observations;

    public function __construct()
    {
        $this->startDate = new \Datetime;
        $this->endDate   = new \Datetime;
    }

    public function __toString()
    {
        return sprintf('%s %s (%s - %s): %s',
            $this->name,
            $this->surname,
            $this->startDate->format('Y-m-d'),
            $this->endDate->format('Y-m-d'),
            implode(', ', $this->rooms)
        );
    }
}

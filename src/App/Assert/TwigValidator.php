<?php

namespace App\Assert;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TwigValidator extends ConstraintValidator
{
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->isContentValid($value)) {
            $this->context->addViolation($constraint->message);

            return false;
        }

        return true;
    }

    private function isContentValid($value)
    {
        try {
            $this->twig->parse($this->twig->tokenize($value));
            return true;
        }
        catch (\Twig_Error_Syntax $e) {
            return false;
        }
    }
}


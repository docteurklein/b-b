<?php

namespace App\Assert;

use Symfony\Component\Validator\Constraint;

class Twig extends Constraint
{
    public $message = 'Invalid twig content';

    public function validatedBy()
    {
        return 'twig';
    }
}


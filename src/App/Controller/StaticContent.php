<?php

namespace App\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormInterface;
use Knp\RadBundle\Controller\Helper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;

class StaticContent
{
    private $mails;
    private $flashes;
    private $responses;

    public function __construct(Helper\Session $flashes, Helper\Response $responses)
    {
        $this->flashes = $flashes;
        $this->responses = $responses;
    }

    public function indexAction()
    {
    }

    public function homepageAction()
    {
        return [
            'images' => array_map(function($path) {
                return str_replace(__DIR__.'/../../../web/', '', $path);
            }, iterator_to_array((new Finder)->in(__DIR__.'/../../../web/images/accueil')->getIterator())),
        ];
    }

    public function aboutAction()
    {
        return [
            'images' => array_map(function($path) {
                return str_replace(__DIR__.'/../../../web/', '', $path);
            }, iterator_to_array((new Finder)->files()->in(__DIR__.'/../../../web/images')->getIterator())),
        ];
    }

    public function editAction(Request $request, $id, FormInterface $form)
    {
        $content = $this->getContentsOr404($id);
        $form->setData(['code' => $content]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->putContents($id, $form->getData()['code']);
            $this->flashes->addFlash('success');

            return $this->responses->redirectToRoute('app_staticcontent_index');
        }

        return [
            'name' => $id,
            'form' => $form->createView(),
        ];
    }

    public function resetAction($id)
    {
        $file = $this->getFilename($id);
        if (!$file or !file_exists($file)) {
            throw new NotFoundHttpException;
        }

        try {
            (new Filesystem)->copy($file.'.bak', $file, true);
            $this->flashes->addFlash('success');
        }catch (\Exception $e) {
            $this->flashes->addFlash('error');
        }

        return $this->responses->redirectToRoute('app_staticcontent_edit', ['id' => $id]);
    }

    private function getContentsOr404($name)
    {
        $file = $this->getFilename($name);
        if (!$file or !file_exists($file)) {
            throw new NotFoundHttpException;
        }

        return file_get_contents($file);
    }

    private function putContents($name, $contents)
    {
        $file = $this->getFilename($name);
        if (!$file or !file_exists($file)) {
            throw new NotFoundHttpException;
        }

        if (md5($contents) !== md5_file($file)) {
            (new Filesystem)->copy($file, $file.'.bak');
        }

        return file_put_contents($file, $contents);
    }

    public function getFilename($name)
    {
        $map = [
            'home'        => __DIR__.'/../Resources/views/StaticContent/homepage.html.twig',
            'about'       => __DIR__.'/../Resources/views/StaticContent/about.html.twig',
            'tarifs'   => __DIR__.'/../Resources/views/StaticContent/tarifs.html.twig',
            'layout'      => __DIR__.'/../Resources/views/StaticContent/layout.html.twig',
        ];

        return @$map[$name];
    }
}

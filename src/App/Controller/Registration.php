<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\RegistrationRepository;
use Symfony\Component\Form\FormInterface;
use App\Entity;
use Knp\RadBundle\Controller\Helper;
use Symfony\Component\HttpFoundation\JsonResponse;

class Registration
{
    private $registrations;
    private $flashes;
    private $responses;
    private $mails;

    public function __construct(RegistrationRepository $registrations, Helper\Session $flashes, Helper\Response $responses, Helper\Mail $mails)
    {
        $this->registrations = $registrations;
        $this->responses = $responses;
        $this->flashes = $flashes;
        $this->mails = $mails;
    }

    public function indexAction()
    {
    }

    public function newAction(Request $request, FormInterface $form)
    {
        $registration = new Entity\Registration;
        $form->setData($registration);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->registrations->save($registration);
            $this->mails->send(
                \Swift_Message::newInstance('new registration', print_r($form->getData(), true))
                ->setFrom('no-reply@example.com')
                ->setTo('no-reply@example.com')
            );
            $this->flashes->addFlash('success');

            return $this->responses->redirectToRoute('app_registration_index');
        }

        return ['form' => $form->createView()];
    }

    public function calendarAction(Request $request)
    {
        $start = \DateTime::createFromFormat('U', $request->query->get('start'));
        $end   = \DateTime::createFromFormat('U', $request->query->get('end'));

        return new JsonResponse(array_map(function($registration) {
            return [
                'title'    => implode(', ', $registration->rooms),
                'allDay'   => true,
                'start'    => $registration->startDate->format('U'),
                'end'      => $registration->endDate->format('U'),
                'editable' => false,
            ];
        }, $this->registrations->findRange($start, $end)));
    }
}

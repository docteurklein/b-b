<?php

namespace App\Controller;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Knp\RadBundle\Controller\Helper;

class Contact
{
    private $form;
    private $mails;
    private $flashes;
    private $responses;

    public function __construct(FormInterface $form, Helper\Session $flashes, Helper\Response $responses, Helper\Mail $mails)
    {
        $this->form = $form;
        $this->mails = $mails;
        $this->flashes = $flashes;
        $this->responses = $responses;
    }

    public function newAction(Request $request)
    {
        $this->form->handleRequest($request);
        if ($this->form->isValid()) {
            $this->mails->send(
                \Swift_Message::newInstance('new contact', print_r($this->form->getData(), true))
                ->setFrom('no-reply@example.com')
                ->setTo('no-reply@example.com')
            );
            $this->flashes->addFlash('success');

            return $this->responses->redirectToRoute('app_registration_index');
        }

        return ['form' => $this->form->createView()];
    }

    public function _formAction(Request $request)
    {
        $request->attributes->set('_view', 'App:Contact:_form');

        return ['form' => $this->form->createView()];
    }
}

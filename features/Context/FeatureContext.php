<?php

namespace Context;

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;

class FeatureContext extends RawMinkContext implements SnippetAcceptingContext
{
}
